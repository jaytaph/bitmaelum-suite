module github.com/bitmaelum/bitmaelum-suite

go 1.13

require (
	github.com/coreos/go-semver v0.3.0
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/gabriel-vasile/mimetype v1.1.0
	github.com/gdamore/tcell v1.3.0
	github.com/go-redis/redis/v8 v8.0.0-beta.2
	github.com/google/uuid v1.1.1
	github.com/gookit/color v1.2.5
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/jessevdk/go-flags v1.4.0
	github.com/juju/fslock v0.0.0-20160525022230-4d5c94c67b4b
	github.com/keybase/go-keychain v0.0.0-20200502122510-cda31fe0c86d
	github.com/manifoldco/promptui v0.7.0
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.11 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/mitchellh/mapstructure v1.3.1 // indirect
	github.com/nightlyone/lockfile v1.0.0
	github.com/olekukonko/tablewriter v0.0.4
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/rivo/tview v0.0.0-20200528200248-fe953220389f
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v1.0.0
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.5.1
	github.com/vtolstov/jwt-go v3.2.1-0.20200120153335-1133da9615b7+incompatible
	github.com/zalando/go-keyring v0.1.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208
	golang.org/x/sys v0.0.0-20200523222454-059865788121 // indirect
	golang.org/x/tools v0.0.0-20200715235423-130c9f19d3fe // indirect
	gopkg.in/ini.v1 v1.57.0 // indirect
	gopkg.in/yaml.v2 v2.3.0
)
